#!/bin/sh
export GUNICORN_WORKERS=2
export GUNICORN_THREADS=2
export GUNICORN_BIND_HOST="0.0.0.0:8000"
export GUNICORN_CMD_ARGS="--chdir app -w $GUNICORN_WORKERS --threads $GUNICORN_THREADS -b $GUNICORN_BIND_HOST"

gunicorn main:app