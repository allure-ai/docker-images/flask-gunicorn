FROM python:3.9

COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

WORKDIR /app

ADD gunicorn_starter.sh gunicorn_starter.sh
ENTRYPOINT ["./gunicorn_starter.sh"]
