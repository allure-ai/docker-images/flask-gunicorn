# Flask-Gunicorn

Provides a base image for flask projects

## Using the image

Put your Flask app on `app/main.py` in the `app` variable.
Your service will be exposed on port `8000`.


## Customization

You can customize the `gunicorn` arguments by setting the environment variable `GUNICORN_CMD_ARGS`.
See gunicorn's docs for more info.

## Author

Muhammad Aditya Hilmy, mhilmy@allure.id